package com.example.controllers;

import com.example.domain.DayEntity;
import com.example.domain.UserEntity;
import com.example.domain.Vocabluary;
import com.example.domain.Word;
import com.example.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

@Controller
public class Training {
    @Autowired
    private UsersRepo usersRepo;
    @Autowired
    private VocabluaryRepo vocabluaryRepo;
    @Autowired
    private WordRepo wordRepo;
    @Autowired
    private DayRepo dayRepo;
    @Autowired
    WordSideRepo wordSideRepo;
    @Autowired
    HelperToControllers helperToControllers;

    private final Scanner sc = new Scanner(System.in);

    @PutMapping("/users/{id}/training")
    public ResponseEntity<String>wordsTrainings(@PathVariable int id){
        System.out.println("Выберите тип тренировки: 1 - Перевод слова на русский, 2 - запись оригинала, " +
                "\n3- Перевод слова из нескольких вариантов, 4- запись оригинала из нескольких вариантов");
        int choise = sc.nextInt();
        boolean translate = choise == 1 || choise == 3;
        System.out.println("Выберите колличество слов на тренировку (-1 - все слова).");
        int wordsCount = sc.nextInt();
        System.out.println("Выберите словарь");
        String str = sc.next();
        str = Character.toUpperCase(str.charAt(0)) + str.substring(1).toLowerCase();
        Optional<Vocabluary>vocabluary = vocabluaryRepo.findByName(str);
        ResponseEntity<String> check = helperToControllers.entityCheck(id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else {
            Optional<UserEntity> userEntity = usersRepo.findById(id);
            System.out.println("Выберите сортировку слов:\n" +
                    "1 - сперва старые, 2 - сперва новые, 3 - без сортировки");
            List<Word> words = wordsRelease(wordsCount, sc.nextInt(), vocabluary.get());
            if (words == null || words.size() < 5)
                System.out.println("Маловато будет");
            else {
                Optional<DayEntity>dayEntity = dayRepo.findAllByDateAndUserEntity(LocalDateTime.now(), userEntity.get());
                DayEntity thisDay = dayEntity.isPresent()? dayEntity.get(): new DayEntity(userEntity.get());
                for (Word i: words) {
                    if (choise <= 2)
                        singleTrain(i, translate, thisDay);
                    else
                        trainFromMany(i, words, translate, thisDay);
                    userEntity.get().setAll_lessons(userEntity.get().getAll_lessons()+1);
                }
            }
            usersRepo.save(userEntity.get());
            return new ResponseEntity<>("Все прошло успешно", HttpStatus.OK);
        }
    }

    private void singleTrain(Word word, boolean translate, DayEntity dayEntity){
        System.out.println("Напишите перевод слова : " +
                (translate? word.getOrigin(): word.getTranslate()));
        String answer = sc.next().toLowerCase();
        if ((translate && answer.equals(word.getTranslate().toLowerCase())) || (!translate && answer.equals(word.getOrigin().toLowerCase()))){
            System.out.println("Правильно");
            dayEntity.setCorrects(dayEntity.getCorrects()+1);
        }else {
            System.out.println("Ошибка...");
            System.out.println("Правильный ответ: " + (translate?word.getTranslate():word.getOrigin()));
            dayEntity.setWrongs(dayEntity.getWrongs()+1);
        }
        word.setLast_learn(LocalDateTime.now());
        wordRepo.save(word);
        dayRepo.save(dayEntity);
    }

    //Версия без фронта. По сути, здесь тоже, что и в метода выше, пользователь все вводит ручками,
    //но теперь у него есть варианты перевода, чтобы он мог найти среди них нужный.
    private void trainFromMany(Word word, List<Word> allWords, boolean transate, DayEntity dayEntity){
        ArrayList<Word> variants = new ArrayList<>();
        while (variants.size() != 3){
            int var = (int)(Math.random() * allWords.size());
            Word isCorrect = allWords.get(var);
            if (!isCorrect.equals(word) && !variants.contains(isCorrect))
                variants.add(isCorrect);
        }
        System.out.println("Варианты: ");
        boolean isShow = false;
        for (Word i : variants) {
            if (!isShow && (((int)(Math.random()*11) < 3) || variants.indexOf(i) == 2)){
                System.out.println(transate ? word.getTranslate(): word.getOrigin());
                isShow = true;
            }
            System.out.println(transate ? i.getTranslate() : i.getOrigin());
        }
        singleTrain(word, transate, dayEntity);
    }

    private List<Word>wordsRelease(int howMach, int orderBy, Vocabluary vocabluary){
        if (howMach == -1){
            if (orderBy == 1)
                return wordSideRepo.findAllByVocabluaryOrderByLastLearn(vocabluary);
            else if (orderBy == 2)
                return wordSideRepo.findAllByVocabluaryOrderByLastLearnDesc(vocabluary);
            else
                return wordRepo.findAllByVocabluary(vocabluary);
        } else {
            if (orderBy == 1)
                return wordSideRepo.findAllByLimitOrderByLastLearn(howMach, vocabluary);
            else if (orderBy == 2)
                return wordSideRepo.findAllByLimmitOrderByLastLearnDesc(howMach, vocabluary);
            else
                return wordSideRepo.findAllByLimit(howMach, vocabluary);
        }
    }
}