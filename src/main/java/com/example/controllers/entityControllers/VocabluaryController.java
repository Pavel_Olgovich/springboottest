package com.example.controllers.entityControllers;

import com.example.controllers.HelperToControllers;
import com.example.controllers.dto.VocabluaryDto;
import com.example.domain.UserEntity;
import com.example.domain.Vocabluary;
import com.example.repos.UsersRepo;
import com.example.repos.VocabluaryRepo;
import com.example.repos.WordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@Controller
public class VocabluaryController {
    @Autowired
    private VocabluaryRepo vocabluaryRepo;

    @Autowired
    private UsersRepo usersRepo;

    @Autowired
    private WordRepo wordRepo;

    @Autowired
    HelperToControllers doh;

    @GetMapping("/users/{user_id}/vocabluaries")
    public ResponseEntity<String> getAllByUserId(@PathVariable int user_id){
        Optional<UserEntity> userEntity = usersRepo.findById(user_id);
        if (userEntity.isEmpty())
            return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
        else {
            List<Vocabluary> vocabluaries = vocabluaryRepo.findVocabluaryByUserEntity(userEntity.get());
            if(vocabluaries.size() == 0)
                return new ResponseEntity<>("Словари не найдены", HttpStatus.NOT_FOUND);
            else
                return new ResponseEntity<>("Словари найдены", HttpStatus.OK);
        }
    }

    @GetMapping("/users/{user_id}/vocabluaries/{id}")
    public ResponseEntity<String> getVocabluaryById(@PathVariable int user_id, @PathVariable int id){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else
                return new ResponseEntity<>("Словарь найден", HttpStatus.OK);
    }

    @PostMapping("/users/{user_id}/vocabluaries")
    public ResponseEntity<String> putNewVocabluaryToUser(@PathVariable int user_id, @RequestBody VocabluaryDto vocDto){
        Optional<UserEntity>userEntity = usersRepo.findById(vocDto.getUserEntityId());
        if (user_id != vocDto.getUserEntityId() || userEntity.isEmpty())
            return new ResponseEntity<>("Неверный пользователь", HttpStatus.NOT_FOUND);
        else {
            Vocabluary vocabluary = new Vocabluary(vocDto.getName(), userEntity.get());
            vocabluaryRepo.save(vocabluary);
            return new ResponseEntity<>("Словарь добавлен", HttpStatus.CREATED);
        }
    }

    @DeleteMapping("/users/{user_id}/vocabluaries/{id}")
    public ResponseEntity<String> deleteVocabluaryById(@PathVariable int user_id, @PathVariable int id){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else {
                doh.deleteAllWordsFromVocabluary(vocabluary.get(), wordRepo);
                vocabluaryRepo.delete(vocabluary.get());
                return new ResponseEntity<>("Словарь удален", HttpStatus.NO_CONTENT);
            }
    }

    @PutMapping("/users/{user_id}/vocabluaries/{id}")
    public ResponseEntity<String>changeNameVocabluaryById(@PathVariable int user_id, @PathVariable int id, @RequestBody VocabluaryDto vocabluaryDto){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else {
            vocabluary.get().setName(vocabluaryDto.getName());
            vocabluaryRepo.save(vocabluary.get());
            return new ResponseEntity<>("Словарь переименован", HttpStatus.OK);
        }
    }
}