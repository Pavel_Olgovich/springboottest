package com.example.controllers.entityControllers;

import com.example.controllers.HelperToControllers;
import com.example.controllers.dto.UserDto;
import com.example.domain.DayEntity;
import com.example.domain.UserEntity;
import com.example.repos.DayRepo;
import com.example.repos.UsersRepo;
import com.example.repos.VocabluaryRepo;
import com.example.repos.WordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Iterator;


@Controller
public class UsersController {
    @Autowired
    private UsersRepo usersRepo;

    @Autowired
    private HelperToControllers doh;

    @Autowired
    private VocabluaryRepo vocabluaryRepo;

    @Autowired
    private WordRepo wordRepo;

    @Autowired
    private DayRepo dayRepo;

   @GetMapping(value = "/users")
    public ResponseEntity<String> getAll() {
        Iterable<UserEntity> users = usersRepo.findAll();
        Iterator<UserEntity> iterator = users.iterator();
        if (!iterator.hasNext())
            return new ResponseEntity<>("Выводить некого", HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>("Пользователи найдены", HttpStatus.OK);
    }

    @GetMapping(value = "/users/{id}")
    public ResponseEntity<UserEntity> getById(@PathVariable int id) {
        System.out.println("Ты попал в метод выбора человека по id");
        Optional<UserEntity> optUserEntity = usersRepo.findById(id);
        if (optUserEntity.isPresent())
            return new ResponseEntity<>(optUserEntity.get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
    }
    @PostMapping(path = "/users")
    public ResponseEntity<UserDto> postUsers(@RequestBody UserDto dto){
       System.out.println("Ты в ветке добавления");
       UserEntity userEntity = new UserEntity();
       userEntity.setName(dto.getName());
       userEntity.setAll_lessons(dto.getAll_lessons());
       userEntity.setDay_count(dto.getDay_count());
       usersRepo.save(userEntity);

       dto.setId(userEntity.getId());

       return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteByName(@PathVariable int id){
       System.out.println("Ты в ветке удаления");
       Optional<UserEntity> optUserEntity = usersRepo.findById(id);
       if (optUserEntity.isEmpty()) {
           return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
       } else {
           doh.deleteAllVocabluariesFromUser(vocabluaryRepo, wordRepo, optUserEntity.get());
           List<DayEntity>daysToDelete = dayRepo.findAllByUserEntity(optUserEntity.get());
           for (DayEntity i: daysToDelete)
               dayRepo.delete(i);
           usersRepo.delete(optUserEntity.get());
           return new ResponseEntity<>("Пользователь удалён", HttpStatus.NO_CONTENT);
       }
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<String> changeInUser(@PathVariable int id, @RequestBody UserDto dto){
       Optional<UserEntity> optionalUserEntity = usersRepo.findById(id);
       if (optionalUserEntity.isEmpty()) {
           return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
       } else {
           optionalUserEntity.get().changeFields(dto);
           usersRepo.save(optionalUserEntity.get());
           return new ResponseEntity<>("Пользователь изменен", HttpStatus.OK);
       }
    }
}