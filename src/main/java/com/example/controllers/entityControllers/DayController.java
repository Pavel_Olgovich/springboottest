package com.example.controllers.entityControllers;

import com.example.controllers.dto.DayDto;
import com.example.domain.DayEntity;
import com.example.domain.UserEntity;
import com.example.repos.DayRepo;
import com.example.repos.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
public class DayController {
    @Autowired
    private DayRepo dayRepo;

    @Autowired
    private UsersRepo usersRepo;

    @GetMapping("/users/{user_id}/days")
    public ResponseEntity<String>findAllUsersDays(@PathVariable int user_id){
        Optional<UserEntity> userEntity = usersRepo.findById(user_id);
        if (userEntity.isEmpty())
            return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
        else {
            List<DayEntity> allDays = dayRepo.findAllByUserEntity(userEntity.get());
            if (allDays.size() == 0)
                return new ResponseEntity<>("Дни не найдены", HttpStatus.NOT_FOUND);
            else
                return new ResponseEntity<>("Дни получены", HttpStatus.OK);
        }
    }

    @PostMapping("/users/{user_id}/days")
    public ResponseEntity<String>postNewData(@PathVariable int user_id){
        Optional<UserEntity> userEntity = usersRepo.findById(user_id);
        if (userEntity.isEmpty())
            return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
        else {
            Optional<DayEntity>isDayExist = dayRepo.findAllByDateAndUserEntity(LocalDateTime.now(), userEntity.get());
            if (isDayExist.isPresent())
                return new ResponseEntity<>("Невозможно добавить новый день, так как он уже есть", HttpStatus.CONFLICT);
            else {
                DayEntity dayEntity = new DayEntity(userEntity.get());
                dayRepo.save(dayEntity);
                return new ResponseEntity<>("День добавлен", HttpStatus.CREATED);
            }
        }
    }

    @PutMapping("/users/{user_id}/days")
    public ResponseEntity<String>putCorrectsWrongsInDay(@PathVariable int user_id, @RequestBody DayDto dto){
        Optional<UserEntity> userEntity = usersRepo.findById(user_id);
        if (userEntity.isEmpty())
            return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
        else {
            Optional<DayEntity> isDayExist = dayRepo.findAllByDateAndUserEntity(LocalDateTime.now(), userEntity.get());
            if (isDayExist.isEmpty())
                return new ResponseEntity<>("Невозможно изменить этот день, так как его нет", HttpStatus.NOT_FOUND);
            else {
                isDayExist.get().setCorrects(dto.getCorrects());
                isDayExist.get().setWrongs(dto.getWrongs());
                dayRepo.save(isDayExist.get());
                return new ResponseEntity<>("День изменен", HttpStatus.OK);
            }
        }
    }
    //DELETE и DET по id методов у контроллера нет, так как пользователь не будет иметь права удалять
    //выборочно дни, а в показе конкретного дня смысла нет
}