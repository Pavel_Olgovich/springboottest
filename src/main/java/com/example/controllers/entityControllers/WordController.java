package com.example.controllers.entityControllers;

import com.example.controllers.HelperToControllers;
import com.example.controllers.dto.WordDto;
import com.example.domain.Vocabluary;
import com.example.domain.Word;
import com.example.repos.UsersRepo;
import com.example.repos.VocabluaryRepo;
import com.example.repos.WordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
public class WordController {
    @Autowired
    private UsersRepo usersRepo;

    @Autowired
    private VocabluaryRepo vocabluaryRepo;

    @Autowired
    private WordRepo wordRepo;

    @Autowired
    private HelperToControllers doh;

    @GetMapping("/users/{user_id}/vocabluaries/{voc_id}/words")
    public ResponseEntity<String> getAllWordsByVocId(@PathVariable int user_id, @PathVariable int voc_id){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(voc_id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else  {
            List<Word> words = wordRepo.findAllByVocabluary(vocabluary.get());
            if (words.size() == 0)
                return new ResponseEntity<>("Слова не найдены", HttpStatus.NOT_FOUND);
            else
                return new ResponseEntity<>("Слова найдены", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/users/{user_id}/vocabluaries/{voc_id}/words/{id}")
    public ResponseEntity<String> getWordInVocById(@PathVariable int user_id, @PathVariable int voc_id, @PathVariable int id){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(voc_id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else  {
            Optional<Word> word = wordRepo.findById((long)id);
            if (word.isEmpty() || word.get().getVocabluary().getId() != voc_id)
                return new ResponseEntity<>("Слово не найдено", HttpStatus.NOT_FOUND);
            else
                return new ResponseEntity<>("Слово получено", HttpStatus.OK);
        }
    }
    //В дальнейшем, при нормальной работе с контроллерами, нужно будет убрать в URL
    //id словаря, или добавить еще один метод, но без урла.

    @PostMapping("/users/{user_id}/vocabluaries/{voc_id}/words")
    public ResponseEntity<String>putWordIntoVocabluary(@PathVariable int user_id, @PathVariable int voc_id, @RequestBody WordDto wordDto){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(voc_id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else{
            Word newWord = new Word(wordDto.getOrigin(), wordDto.getTranslate(), vocabluary.get());
            wordRepo.save(newWord);
            return new ResponseEntity<>("Слово добавлено", HttpStatus.CREATED);
        }
    }

    @DeleteMapping("/users/{user_id}/vocabluaries/{voc_id}/words/{id}")
    public ResponseEntity<String>deleteWordInVocabluary(@PathVariable int user_id, @PathVariable int voc_id, @PathVariable int id){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(voc_id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else  {
            Optional<Word> word = wordRepo.findById((long)id);
            if (word.isEmpty() || word.get().getVocabluary().getId() != voc_id)
                return new ResponseEntity<>("Слово не найдено", HttpStatus.NOT_FOUND);
            else {
                wordRepo.delete(word.get());
                return new ResponseEntity<>("Слово удалено", HttpStatus.OK);
            }
        }
    }

    @PutMapping("/users/{user_id}/vocabluaries/{voc_id}/words/{id}")
    public ResponseEntity<String>changeWordFromVoc(@PathVariable int user_id, @PathVariable int voc_id, @PathVariable int id,
                                                   @RequestBody WordDto wordDto){
        Optional<Vocabluary> vocabluary = vocabluaryRepo.findById(voc_id);
        ResponseEntity<String> check = doh.entityCheck(user_id, usersRepo, vocabluary);
        if (!check.getStatusCode().equals(HttpStatus.OK))
            return check;
        else {
            Optional<Word> word = wordRepo.findById((long)id);
            if (word.isEmpty())
                return new ResponseEntity<>("Слово не найдено", HttpStatus.NOT_FOUND);
            else {
                if (wordDto.isForLastLearn) {
                    word.get().setLast_learn(LocalDateTime.now());
                }
                else {
                    word.get().wordChange(wordDto);
                }
                wordRepo.save(word.get());
                return new ResponseEntity<>("Слово изменено",  HttpStatus.OK);
            }
        }
    }
}