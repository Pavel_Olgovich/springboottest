package com.example.controllers;

import com.example.domain.UserEntity;
import com.example.domain.Vocabluary;
import com.example.domain.Word;
import com.example.repos.UsersRepo;
import com.example.repos.VocabluaryRepo;
import com.example.repos.WordRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class HelperToControllers {

    public void deleteAllWordsFromVocabluary(Vocabluary vocabluary, WordRepo wordRepo){
            List<Word>words = wordRepo.findAllByVocabluary(vocabluary);
                for (Word i: words)
                    wordRepo.delete(i);
    }

    public void deleteAllVocabluariesFromUser(VocabluaryRepo vocabluaryRepo, WordRepo wordRepo, UserEntity userEntity){
        List<Vocabluary> vocabluaries = vocabluaryRepo.findVocabluaryByUserEntity(userEntity);
        for (Vocabluary i : vocabluaries){
            deleteAllWordsFromVocabluary(i, wordRepo);
            vocabluaryRepo.delete(i);
        }
    }
    public ResponseEntity<String> entityCheck(int user_id, UsersRepo usersRepo, Optional<Vocabluary> vocabluary){
        Optional<UserEntity>userEntity = usersRepo.findById(user_id);
        if (userEntity.isEmpty())
            return new ResponseEntity<>("Пользователь не найден", HttpStatus.NOT_FOUND);
        else if (vocabluary.isEmpty() || userEntity.get().getId() != vocabluary.get().getUserEntity().getId())
            return new ResponseEntity<>("Словарь не найден", HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>("", HttpStatus.OK);
    }
}