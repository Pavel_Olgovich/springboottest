package com.example.controllers.dto;

import lombok.Data;

@Data
public class DayDto {
    private int corrects;
    private int wrongs;
}
