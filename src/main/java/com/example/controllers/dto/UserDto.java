package com.example.controllers.dto;

import lombok.Data;

@Data
public class UserDto {

    private int id;

    private String name;

    private int all_lessons;

    private int day_count;

}
