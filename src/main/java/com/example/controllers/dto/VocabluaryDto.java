package com.example.controllers.dto;

import lombok.Data;

@Data
public class VocabluaryDto {
    private int id;

    private String name;

    private int userEntityId;
}
