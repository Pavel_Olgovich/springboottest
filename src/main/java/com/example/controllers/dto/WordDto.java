package com.example.controllers.dto;

import lombok.Data;

@Data
public class WordDto {
    private String origin;
    private String translate;
    public boolean isForLastLearn;
}
