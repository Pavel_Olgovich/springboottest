package com.example.repos;
import com.example.domain.UserEntity;
import com.example.domain.Vocabluary;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface VocabluaryRepo extends CrudRepository<Vocabluary, Integer> {
    List<Vocabluary> findVocabluaryByUserEntity(UserEntity userEntity);
    Optional<Vocabluary> findByName(String name);
}
