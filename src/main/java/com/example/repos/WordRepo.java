package com.example.repos;
import com.example.domain.Vocabluary;
import com.example.domain.Word;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WordRepo extends CrudRepository<Word, Long>{
    List<Word> findAllByVocabluary(Vocabluary vocabluary);
}