package com.example.repos;

import com.example.domain.Vocabluary;
import com.example.domain.Word;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class WordSideRepo {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Word>findAllByLimit(int limit, Vocabluary vocabluary){
        return entityManager.createQuery("SELECT a FROM Word a WHERE a.vocabluary = :vocabluary", Word.class).setParameter("vocabluary", vocabluary).setMaxResults(limit).getResultList();
    }

    public List<Word>findAllByLimitOrderByLastLearn(int limit, Vocabluary vocabluary){
        return entityManager.createQuery("SELECT a FROM Word a WHERE a.vocabluary = :vocabluary ORDER BY a.last_learn", Word.class).setParameter("vocabluary", vocabluary).setMaxResults(limit).getResultList();
    }
    public List<Word>findAllByLimmitOrderByLastLearnDesc(int limit, Vocabluary vocabluary){
        return entityManager.createQuery("SELECT a FROM Word a WHERE a.vocabluary = :vocabluary order by a.last_learn DESC", Word.class).setParameter("vocabluary", vocabluary).setMaxResults(limit).getResultList();
    }
    public List<Word>findAllByVocabluaryOrderByLastLearn(Vocabluary vocabluary){
        return entityManager.createQuery("SELECT a from Word a where a.vocabluary = :vocabluary order by a.last_learn", Word.class).setParameter("vocabluary", vocabluary).getResultList();
    }
    public List<Word>findAllByVocabluaryOrderByLastLearnDesc(Vocabluary vocabluary){
        return entityManager.createQuery("SELECT a from Word a where a.vocabluary = :vocabluary order by a.last_learn DESC", Word.class).setParameter("vocabluary", vocabluary).getResultList();
    }
}
