package com.example.repos;

import com.example.domain.DayEntity;
import com.example.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DayRepo extends CrudRepository<DayEntity, Long> {
    List<DayEntity>findAllByUserEntity(UserEntity userEntity);
    Optional<DayEntity> findAllByDateAndUserEntity(LocalDateTime ldt, UserEntity userEntity);
}
