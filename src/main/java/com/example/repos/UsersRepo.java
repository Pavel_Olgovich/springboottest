package com.example.repos;
import com.example.domain.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UsersRepo extends CrudRepository<UserEntity, Integer>{
}
