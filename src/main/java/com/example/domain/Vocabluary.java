package com.example.domain;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "vocabluary")
@Data
public class Vocabluary {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity userEntity;

    public Vocabluary(){}
    public Vocabluary(String name, UserEntity userEntity){
        this.name = name;
        this.userEntity = userEntity;
    }
}
