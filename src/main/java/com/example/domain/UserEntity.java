package com.example.domain;

import javax.persistence.*;

import com.example.controllers.dto.UserDto;
import lombok.Data;

@Entity
@Table(name = "users")
@Data
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "all_lessons")
    private int all_lessons;

    @Column(name = "day_count")
    private int day_count;

    public void changeFields(UserDto dto){
        name = dto.getName();
        all_lessons = dto.getAll_lessons();
        day_count = dto.getDay_count();
    }
}