package com.example.domain;
import com.example.controllers.dto.WordDto;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "words")
@Data
public class Word {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "origin")
    private String origin;
    @Column(name = "translate")
    private String translate;

    @Column(name = "last_learn")
    private LocalDateTime last_learn;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "voc_id", nullable = false)
    private Vocabluary vocabluary;

    public Word(){}

    public Word(String origin, String translate, Vocabluary vocabluary) {
        this.origin = origin;
        this.translate = translate;
        this.vocabluary = vocabluary;
    }
    public void wordChange(WordDto wordDto){
        this.origin = wordDto.getOrigin();
        this.translate = wordDto.getTranslate();
    }
}