package com.example.domain;

import lombok.Data;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "day")
@Data
public class DayEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "corrects")
    private int corrects;

    @Column(name = "wrongs")
    private int wrongs;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    private UserEntity userEntity;

    public DayEntity(){}
    public DayEntity(UserEntity userEntity){
        date = LocalDateTime.now();
        this.userEntity = userEntity;
    }
}
